import React, {Component, Fragment} from 'react';
import axios from '../../axios-search';
import {NavLink} from "react-router-dom";
import './Movies.css'
import withErrorHendler from "../../hoc/withErrorHendler";


class Movies extends Component {

  state = {
    movies: null
  };

  loadMovie = () =>{
    let url = 'movies.json';
    axios.get(url).then(response=>{
      if (response.data !== null){
        const movies = Object.keys(response.data).map(id => {
          return {...response.data[id], id};
        });
        this.setState({movies});
      }else{
        this.setState({movies: []})
      }
    })
  };
  componentDidMount(){
    this.loadMovie()
  }
  deleteMovie = (id) =>{
    axios.delete('movies/' +  id + '.json').then(()=>{
      this.loadMovie();
    })
  };
  render() {
    let movies = null;
    if (this.state.movies){
      movies = this.state.movies.map(movie=>{
        return(
          <div className='ToDo-Output' key={movie.id}>
            <p>Movie: {movie.text}</p>
            <button onClick={()=>this.deleteMovie(movie.id)}>delete</button>
          </div>
        )
      })
    }

    return (
      <Fragment>
        <NavLink to='/movies/add' className='Go-To' exact>Go to added</NavLink>
        <h1>Movies List</h1>
        {movies}
      </Fragment>
    );
  }
}

export default withErrorHendler(Movies, axios) ;