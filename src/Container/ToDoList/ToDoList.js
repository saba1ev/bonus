import React, {Component, Fragment} from 'react';
import FormToDo from "../../component/FormToDo/FormToDo";
import axios from '../../axios-search';
import './ToDoList.css';

class ToDoList extends Component {
  state={
    toDo: null,
  };
  loadPost = () => {
    let url = 'todolist.json';
    const categoryId = this.props.match.params.categoryId;
    if (categoryId) {
      url += `?orderBy="category"&equalTo="${categoryId}"`;
    }
    axios.get(url).then(response=>{
      if (response.data !== null){
        const toDo = Object.keys(response.data).map(id => {
          return {...response.data[id], id};
        });
        this.setState({toDo});
      }else{
        this.setState({toDo: []})
      }
    })
  };
  addPostsToDo = post => {
    axios.post('todolist.json', post).then(() => {
      this.loadPost();
    })
  };
  deletePost = (id) =>{
    axios.delete('todolist/'+ id +'.json').then(()=>{
      this.loadPost();
    })
  };
  componentDidMount(){
    this.loadPost();
  }
  componentDidUpdate(prevProps){
    if (this.props.match.params.categoryId !== prevProps.match.params.categoryId){
      this.loadPost()
    }
  }
  render() {
    let toDo = null;
    if (this.state.toDo){
      toDo = this.state.toDo.map(post=>{
        return(
          <div className='ToDo-Output' key={post.id}>
            <p>ToDoText: {post.text}</p>
            <button onClick={()=>this.deletePost(post.id)}>delete</button>
          </div>
        )
      })
    } else {
      return(<div>Loading page...</div>)
    }
    return (
      <Fragment>
        <FormToDo button='Add new To Do List'
                  title='TodoList'
                  onSubmit={this.addPostsToDo}/>
        {toDo}
      </Fragment>
    );
  }
}

export default ToDoList;