import React, {Component, Fragment} from 'react';
import Modal from "../component/UI/Modal/Modal";
import Spinner from "../component/UI/Spinner/Spinner";


const withErrorHendler = (WrappedComponent, axios) =>{
  return class WithErrorHendlerHOC extends Component{
    constructor(props){
      super(props);

      this.state = {
        error: null,
        loading: false
      };


      axios.interceptors.request.use(req => {
        this.setState({loading: true});
        return req
      })

      axios.interceptors.response.use(res=> {
        this.setState({loading: false});
        return res
      }, error => {
        this.setState({error, loading: false});
        throw error;
      });
    }

    errorDissmis = () =>{
      this.setState({error: null})
    };

    render() {
      return (
        <Fragment>
          {this.state.loading ? <Spinner/> : null}
          <Modal show={this.state.error}>
            {this.state.error && this.state.error.message}
            <button onClick={this.errorDissmis}>Close</button>
          </Modal>
          <WrappedComponent {...this.props}/>
        </Fragment>

      )

    }

  }
}

export default withErrorHendler;