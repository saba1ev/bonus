import axios from 'axios';

const basic = axios.create({
  baseURL: 'https://bunus-sobolev.firebaseio.com/'
});

basic.interceptors.request.use(req =>{
  console.log('[In request interceptors]', req);
  return req;
});


export default basic;