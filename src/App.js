import React, {Component, Fragment} from 'react';
import {Switch,Route, NavLink as RouterNavLink} from "react-router-dom";
import {
  Collapse, Container, Nav,
  Navbar, NavbarBrand, NavbarToggler, NavItem,
  NavLink
} from "reactstrap";
import ToDoList from "./Container/ToDoList/ToDoList";
import './App.css';
import Movies from "./Container/Movies/Movies";
import addMovies from './component/addMovies/addMovies'


class App extends Component {
  render() {
    return (
      <Fragment>
        <Navbar color="light" light expand="md">
          <NavbarBrand>Bonus project</NavbarBrand>
          <NavbarToggler />
          <Collapse isOpen navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink tag={RouterNavLink} to='/' exact>To do list</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={RouterNavLink} to='/movies'>Movie</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <Container>
          <Switch>
            <Route exact path='/' component={ToDoList}/>
            <Route path='/movies/add' component={addMovies}/>
            <Route path='/movies' component={Movies}/>

            <Route render={()=><h1>Not Found...</h1>}/>
          </Switch>
        </Container>

      </Fragment>

    );
  }
}

export default App;
