import React, {Component} from 'react';
import FormToDo from "../FormToDo/FormToDo";
import axios from "../../axios-search";

class AddMovies extends Component {
  addMovie = movie => {
    axios.post('movies.json', movie).then(() => {
      this.props.history.replace('/movies')
    })
  };
  render() {
    return (
      <div>
        <h1>Add new Film</h1>
        <FormToDo title='Movies'
                  button='Add new Movie'
                  onSubmit={this.addMovie}/>
      </div>
    );
  }
}

export default AddMovies;