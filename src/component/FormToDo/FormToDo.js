import React, {Component} from 'react';
import {Button, Col, Container, Form, FormGroup, Input, Label} from "reactstrap";

class FormToDo extends Component {
  state={
    text: ''
  };

  clickHendler = event =>{
    const {name, value} = event.target;
    this.setState({[name]: value})
  };
  submitHandler = event => {
    event.preventDefault();
    this.props.onSubmit({...this.state})
  };

  render() {
    return (
      <div>
        <Container>
          <Form onSubmit={this.submitHandler}>
            <FormGroup row>
              <Col sm={1.2}>
                <Label for="exampleToDo">{this.props.title}</Label>
              </Col>
              <Col sm={6}>
                <Input type='text' name='text' placeholder='Write....'
                       value={this.state.text} onChange={this.clickHendler}
                />
              </Col>
              <Button color='success' type='submit'>{this.props.button}</Button>
            </FormGroup>

          </Form>
        </Container>
      </div>
    );
  }
}

export default FormToDo;